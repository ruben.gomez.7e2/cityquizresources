package cat.itb.cityquiz.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.screen.intro.IntroFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

    }
}
