package cat.itb.cityquiz.presentation.screen.questions;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class CityQuizViewModel extends ViewModel {
    private GameLogic gameLogic;
    //private Game game;
    private MutableLiveData<Game> mutableGame = new MutableLiveData<>();
    private Game game;

    public CityQuizViewModel() {
    }

    public void startGame(){
        gameLogic = RepositoriesFactory.getGameLogic();
        this.game = gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);
        mutableGame.postValue(game);
    }

    public int getScore(){
        return game.getNumCorrectAnswers();
    }

    public List<City> getCityList(){
        return game.getCurrentQuestion().getPossibleCities();
    }

    public void checkAnswer(int numButton){
        game = gameLogic.answerQuestions(game,numButton);
        mutableGame.postValue(game);

    }


    public String getFileName() {
        return ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
    }
    public boolean gameIsFinished(){
        return game.isFinished();
    }

    public MutableLiveData<Game> getMutableGame() {
        return mutableGame;
    }
}
