package cat.itb.cityquiz.presentation.screen.intro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.screen.questions.CityQuizViewModel;

public class IntroFragment extends Fragment {

    private CityQuizViewModel mViewModel;

    public static IntroFragment newInstance() {
        return new IntroFragment();
    }

    MaterialButton button;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.intro__fragment, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button = view.findViewById(R.id.startButton);
        button.setOnClickListener(this::navToQuiz);
    }

    private void navToQuiz(View view) {
        mViewModel.startGame();
        Navigation.findNavController(view).navigate(R.id.goToQuizFragment);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);

    }

}
