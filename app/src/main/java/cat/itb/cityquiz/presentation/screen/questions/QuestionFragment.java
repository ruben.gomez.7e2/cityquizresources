package cat.itb.cityquiz.presentation.screen.questions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;

public class QuestionFragment extends Fragment {

    ImageView image;
    MaterialButton button1, button2, button3, button4, button5, button6;
    View view;

    private CityQuizViewModel mViewModel;

    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        ButterKnife.bind(this, view);

        image = view.findViewById(R.id.imageView);
        button1 = view.findViewById(R.id.answer1);
        button2 = view.findViewById(R.id.answer2);
        button3 = view.findViewById(R.id.answer3);
        button4 = view.findViewById(R.id.answer4);
        button5 = view.findViewById(R.id.answer5);
        button6 = view.findViewById(R.id.answer6);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getMutableGame().observe(this, this::updateGame);
    }

    @OnClick({R.id.answer1, R.id.answer2, R.id.answer3, R.id.answer4, R.id.answer5, R.id.answer6})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.answer1:
                checkCorrect(0);
                break;
            case R.id.answer2:
                checkCorrect(1);
                break;
            case R.id.answer3:
                checkCorrect(2);
                break;
            case R.id.answer4:
                checkCorrect(3);
                break;
            case R.id.answer5:
                checkCorrect(4);
                break;
            case R.id.answer6:
                checkCorrect(5);
                break;
        }
    }

    public void updateGame(Game game) {
        if (mViewModel.gameIsFinished()) {
            Navigation.findNavController(view).navigate(R.id.endOfGame);
        } else {
            List<City> answers = mViewModel.getCityList();
            String fileName = mViewModel.getFileName();
            int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
            image.setImageResource(resId);
            button1.setText(answers.get(0).getName());
            button2.setText(answers.get(1).getName());
            button3.setText(answers.get(2).getName());
            button4.setText(answers.get(3).getName());
            button5.setText(answers.get(4).getName());
            button6.setText(answers.get(5).getName());
        }


    }

    public void checkCorrect(int numButton) {
        mViewModel.checkAnswer(numButton);


    }
}

